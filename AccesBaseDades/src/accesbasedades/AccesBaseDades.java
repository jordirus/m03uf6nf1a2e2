/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesbasedades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Danel i Jordi
 */
public class AccesBaseDades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        //Configuració dels parametres d'accés a la bbdd
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String user="danel";
        String pass="544728";
        int option=0;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            try(Connection con=DriverManager.getConnection(url,user,pass)) {
                while(option!=6) {
                    System.out.println("Seleccioni una opció del menu: ");
                    System.out.println("------------------------------ ");
                    System.out.println("1. Mostrar totes les companyies");
                    System.out.println("2. Mostrar companyia per nom");
                    System.out.println("3. Afegir nova companyia");
                    System.out.println("4. Modificar companyia");
                    System.out.println("5. Esborrar companyia");
                    System.out.println("6. Sortir");
                    option = teclat.nextInt();
                    switch (option) {
                        case 1: PreparedStatement stmt=con.prepareStatement("SELECT ID, NOM FROM COMPANYIA");
                                ResultSet rst=stmt.executeQuery();
                                System.out.println("ID   | NOM             ");
                                System.out.println("-----------------------");
                                while(rst.next()) {
                                System.out.printf(" %d   | %s\n", rst.getLong(1), rst.getString(2));
                                }
                            break;
                        case 2: PreparedStatement cons=con.prepareStatement("SELECT ID, NOM FROM COMPANYIA WHERE NOM=?");
                                System.out.println("Nom de la companyia per buscar: ");
                                String nom=teclat.next();
                                cons.setString(1,nom.toUpperCase());
                                ResultSet rest=cons.executeQuery();
                                System.out.println("ID   | NOM             ");
                                System.out.println("-----------------------");
                                while(rest.next()) {
                                System.out.printf(" %d   | %s\n", rest.getLong(1), rest.getString(2));
                                }
                            break;
                        case 3: PreparedStatement agr=con.prepareStatement("INSERT INTO COMPANYIA(ID, NOM) VALUES (?)");
                                System.out.println("Nom de la compayia(Sense espais): ");
                                nom=teclat.next();
                                agr.setString(2, nom.toUpperCase());
                                ResultSet agregat=agr.executeQuery();
                            break;
                        case 4: 
                            break;
                        case 5: 
                            break;
                    }
                } 
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccesBaseDades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
}
 